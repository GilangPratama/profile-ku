from django.shortcuts import render

# Create your views here.

def profil(request):
    return render(request, 'profil.html')

def gilang(request):
    return render(request, 'Gilang.html')

def about(request):
    return render(request, 'About.html')