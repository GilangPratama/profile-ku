from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.profil, name='profil'),
    path('gilang/', views.gilang, name='gilang'),
    path('about/', views.about, name='about'),
]